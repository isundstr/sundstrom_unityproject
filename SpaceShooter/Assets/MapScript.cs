﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapScript : MonoBehaviour {

	public static MapScript mS;

	public List<List<int>> map;

	int height = 2;

	public GameObject wallPrefab;

	Vector2 origin = new Vector2 (-2f, -2f);

	// Use this for initialization
	void Start () {

		mS = this;

		map = new List<List<int>> ();
		map.Add (new List<int> () {0, 1, 0, 1, 2});
		map.Add (new List<int> () {1, 2, 0, 0, 0});
		map.Add (new List<int> () {0, 2, 0, 1, 0});
		map.Add (new List<int> () {1, 0, 0, 1, 1});
		map.Add (new List<int> () {1, 0, 0, 0, 1});
		GenerateMap ();

	}
	
	// Update is called once per frame
	void Update () {

	}

	void GenerateMap(){
		for (int y = 0; y < map.Count; y++) {

			for (int x = 0; x < map [y].Count; x++) {
				if (map [y] [x] == 0)
					continue;
				float scale = 2f;
				Vector3 position = new Vector3 ((x + origin.x) * scale, wallPrefab.transform.position.y, (y + origin.y) * scale);
				Instantiate (wallPrefab, position, Quaternion.identity, transform);
			}

		}
	}
}
