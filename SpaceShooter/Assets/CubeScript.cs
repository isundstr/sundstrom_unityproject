﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeScript : MonoBehaviour {

	public Transform innerCube;
	Vector3 defaultScale;

	Vector3 hopStart;
	Vector3  hopEnd;

	int x = 0;
	int y = 0;
	// Use this for initialization
	void Start () {
		defaultScale = innerCube.localScale;

		hopStart = hopEnd = transform.position;

		StartCoroutine (ReturnToShape ());

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.UpArrow)){
			Move(new Vector2(1f, 0f));
		}else
		if (Input.GetKeyDown(KeyCode.DownArrow)){
					Move(new Vector2(-1f, 0f));
		}else		
		if (Input.GetKeyDown(KeyCode.RightArrow)){
							Move(new Vector2(0f, -1f));
		}else		
		if (Input.GetKeyDown(KeyCode.LeftArrow)){
									Move(new Vector2(0f, 1f));
		}
	}

	void Move(Vector2 movement){

		float moveAdj = 2f;

		x += (int)(movement.x);
		y += (int)(movement.y);

		float endY = (float)(MapScript.mS.map [x] [y]) * 2f;

		print("x: " + x + " y: " + y + "MAP VAL: " + MapScript.mS.map [x] [y]);

		innerCube.localScale = new Vector3 (defaultScale.x, defaultScale.y * 3f, defaultScale.z);

		transform.position = hopEnd;

		hopStart = transform.position;
		hopEnd = transform.position + new Vector3(movement.x * moveAdj, 0f, movement.y * moveAdj);
		hopEnd = new Vector3 (hopEnd.x, endY, hopEnd.z);


		StopCoroutine ("Hop");
		StartCoroutine ("Hop");
	}

	IEnumerator ReturnToShape(){
		while (true) {

			innerCube.localScale = Vector3.Lerp (innerCube.localScale, defaultScale, .1f);

			yield return new WaitForSeconds (1f / 60f);
		}
	}

	IEnumerator Hop(){

		float lerp = 0f;
		float frameRate = 1f / 60f;

		float hopHeight = 2.5f;

		while (lerp < 1f) {
			lerp = Mathf.MoveTowards (lerp, 1f, frameRate * 8f);

			float jumpHeight = Mathf.Abs (lerp - .5f) * 2f;
			jumpHeight = 1f - jumpHeight;

			jumpHeight = jumpHeight * hopHeight;
			jumpHeight = jumpHeight + hopStart.y;


			Vector3 xZPos = Vector3.Lerp (hopStart, hopEnd, lerp);

			transform.position = new Vector3 (xZPos.x, jumpHeight, xZPos.z);

			yield return new WaitForSeconds (frameRate);
		}
		transform.position = hopEnd;

	}
}
